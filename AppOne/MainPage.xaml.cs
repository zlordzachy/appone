﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace AppOne
{
    public sealed partial class MainPage : Page
    {
        private const string DatabaseName = "db.txt";
        private StorageFile DatabaseFile;
        private List<Record> recordList = new List<Record>();

        public MainPage()
        {
            this.InitializeComponent();

            AudioCapturePermissions.RequestMicrophonePermission();

            CreateOpenLoad();
        }

        private async void CreateOpenLoad()
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            DatabaseFile = await storageFolder.CreateFileAsync(DatabaseName, CreationCollisionOption.OpenIfExists);

            IList<String> records = await FileIO.ReadLinesAsync(DatabaseFile);
            foreach (string recordJson in records.ToList())
            {
                var record = JsonConvert.DeserializeObject<Record>(recordJson);
                recordListView.Items.Add(record);
                recordList.Add(record);
            }           
        }

        private void AddRecordItem_Click(object sender, RoutedEventArgs e)
        {
            var titleText = titleEditText.Text;
            var recordText = recordEditText.Text;

            var record = new Record(Guid.NewGuid().ToString(), titleText, recordText, DateTime.Now);
            record.Type = (String)TypeComboBox.SelectedItem;
            record.TypeImage = typeToImage((String)TypeComboBox.SelectedItem);
            record.Pritority = priorityToColor((String)PriorityComboBox.SelectedItem);

            recordListView.Items.Add(record);
            saveToDatabase(record);
        }

        private async void RecordSpeechButton_Click(object sender, RoutedEventArgs e)
        {
            // Create an instance of SpeechRecognizer.
            var speechRecognizer = new Windows.Media.SpeechRecognition.SpeechRecognizer();

            // Compile the dictation grammar by default.
            await speechRecognizer.CompileConstraintsAsync();

            // Start recognition.
            Windows.Media.SpeechRecognition.SpeechRecognitionResult speechRecognitionResult = await speechRecognizer.RecognizeWithUIAsync();

            recordEditText.Text = speechRecognitionResult.Text;
        }

        private async void saveToDatabase(Record record)
        {
            recordList.Add(record);

            var recordJson = JsonConvert.SerializeObject(record) + "\n";
            await FileIO.AppendTextAsync(DatabaseFile, recordJson);

            titleEditText.Text = "";
            recordEditText.Text = "";
            PriorityComboBox.SelectedIndex = 0;
            TypeComboBox.SelectedIndex = 0;
        }

        private async void Delete_Click(object sender, RoutedEventArgs e)
        {
            var UUID = ((Button)sender).Tag.ToString();
            var recordToDelete = recordList.Find(x => x.UUID == UUID);

            recordList.Remove(recordToDelete);
            recordListView.Items.Remove(recordToDelete);

            var lines = new List<String>();
            foreach(Record record in recordList)
            {
                lines.Add(JsonConvert.SerializeObject(record));
            }

            await FileIO.WriteLinesAsync(DatabaseFile, lines);
        }

        private void RecordListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // TODO: ItemClick??
        }

        private string priorityToColor(string priority)
        {
            if (priority == "Důležité")
            {
                return "Red";
            } else if (priority == "Normální")
            {
                return "Green";
            } else
            {
                return "Beige";
            }
        }

        private string typeToImage(string type)
        {
            if (type == "Škola")
            {
                return "Assets/school.png";
            }
            else if (type == "Práce")
            {
                return "Assets/work.png";
            }
            else if (type == "Lednička")
            {
                return "Assets/freezer.png";
            }
            else
            {
                return "Assets/home.png";
            }
        }
    }
}
